<?php


/**
 *      @file
 *      realname.adminmultipl.inc
 *      this file will be delete and it's code add to the original module.
 *
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



function realname_admin_fields_multiple($form_state) {
  $form = array();

  $module = variable_get('realname_profile_module', NULL);

  // Do we have a module set yet?
  if (!$module) {
    drupal_goto('admin/user/realname/multiplemodule');
  }

  //$type define que es lo que tenemos si es core profile, o si son tipos de contenidos.
  //if it's profile the return should be an array with profile - profile options
  $type = ($module == 'coreprofile') ? array('coreprofile' => 'coreprofile') : _realname_clean_simple_array(variable_get('realname_profile_type', NULL));

  $current = array();
  foreach ($type as $keytypes) {

    //$current recupera los campos seleccionados previamete
    $elements = variable_get('realname_fields_'.$keytypes, array());

    foreach($elements as $keyelements => $elementsvalue) {
      $current[$keyelements] = $elementsvalue;
    }
  }

  //get the fields.
  $path = ($module == 'coreprofile')? 'profile' : 'content_profile';
  include_once(drupal_get_path('module', 'realname') .'/realname_'. $path .'.inc');
  $profile_fields = call_user_func('realname_'. $path .'_get_fields', $current, $type);

  $links = $profile_fields['links'];
  asort($links);

  //store in the form_state array the $type content.
  $form_state['storage'] = $type;

  $a = 0;

  //aquí debería empezar el bucle con foreach.
  foreach ($profile_fields['fields'] as $fields ) {

    uasort($fields, '_realname_sort');

    $form['heading'.$a] = array(
      '#type' => 'item',
      '#value' => t('You can configure the fields of this content type.'),
    );

    //comienzo de la tabla
    $form['start_table_'.$a] = array(
      '#type' => 'markup',
      '#value' => '<table><tr><th>Select</th><th>Field name</th><th>Weight</th></tr>',
    );

    $i = 0;

    foreach ($fields as $f_name => $values) {

      $form['field_select_'.$values['origin'].'_'.  $i] = array(
        '#type' => 'checkbox',
        '#default_value' => $values['selected'],
        '#prefix' => '<tr><td align="center">',
        '#suffix' => '</td>',
      );

      $form['field_name_'.$values['origin'].'_'.  $i] = array(
        '#type' => 'hidden',
        '#value' => $f_name,
      );

      $form['field_title_'.$values['origin'].'_'.  $i] = array(
        '#type' => 'item',
        '#value' => $values['title'],
        '#prefix' => '<td>',
        '#suffix' => '</td>',
      );

      $form['field_weight_'.$values['origin'].'_'.  $i] = array(
        '#type' => 'weight',
        '#delta' => 10,
        '#default_value' => $values['weight'],
        '#prefix' => '<td>',
        '#suffix' => '</td></tr>',
      );
      $i++;
    }

    $form['end_table'.$a] = array(
      '#type' => 'markup',
      '#value' => '</table>',
    );

    $form['realname_pattern_'.$values['origin']] = array(
      '#type' => 'textfield',
      '#field_prefix' => '<strong>'. t('Name Pattern') .'</strong> ',
      '#description' => t('This determines how the fields will be used to create the "Real name." Use "%1" to refer to the first field, "%2" to refer to the second, etc.'),
      '#size' => 30,
      '#default_value' => variable_get('realname_pattern_'. $values['origin'], '%1'),
    );

    // If there were any URL fields, give a home page option.
    $form['homepage'.$a] = array(
      '#type' => 'fieldset',
      '#title' => t('Homepage options at @types', array('@types' => $values['origin'])),
      '#description' => t('There were URL fields in the profile. If one of these is a personal homepage link, you may choose to link to it rather than the user profile. Choose which field to use.'),
      '#collapsible' => TRUE,
      '#collapsed' => count($links) == 0,
      '#access' => !empty($links),
    );

    $form['homepage'.$a]['realname_homepage_'.$values['origin']] = array(
      '#type' => 'select',
      '#options' => array('' => t('- None -')) + $links[$values['origin']],
      '#title' => t('Link to homepage'),
      '#description' => t('Select a personal homepage link, if appropriate.'),
      '#default_value' => variable_get('realname_homepage_'. $values['origin'], ''),
    );

    $form['homepage'.$a]['realname_nofollow_'.$values['origin']] = array(
      '#type' => 'checkbox',
      '#title' => t('Spam link deterrent'),
      '#description' => t('If enabled, Drupal will add rel="nofollow" to all links, as a measure to reduce the effectiveness of spam links. Note: this will also prevent valid links from being followed by search engines, therefore it is likely most effective when enabled for anonymous users.'),
      '#default_value' => variable_get('realname_nofollow_'. $values['origin'], FALSE),
    );

    $a++;
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
;

  return $form;
}

/**
 * Form submit handler.
 */
function realname_admin_fields_multiple_submit($form, &$form_state) {

  $fields = array();

  foreach ($form_state['storage'] as $storage) {

    $i = 0;
    // Run the form values to get all the fields they want.
    while (($form_state['values']['field_select_'. $storage .'_'. $i] == 1)) {

      //para reordenar los campos una vez se decide el peso.
      if ($form_state['values']['field_select_'. $storage. '_'. $i]) {

        $fields[$storage][] = array(
          'title' => $form_state['values']['field_name_'. $storage. '_'. $i],
          'weight' => $form_state['values']['field_weight_'. $storage. '_'. $i],
        );
      }
      $i++;
    }

    // A little hoop jumping to sort right.
    uasort($fields[$storage], '_realname_sort');

    //delete the variable by the way.
    unset($realname_fields);

    $realname_fields  = array();

    foreach ($fields[$storage] as $key => $values) {
      $realname_fields[$values['title']] = $values['weight'];
    }

    // Okay, save the stuff.
    variable_set('realname_fields_'. $storage, $realname_fields);
    variable_set('realname_pattern_'. $storage, $form_state['values']['realname_pattern_'. $storage]);
    variable_set('realname_homepage_'. $storage, $form_state['values']['realname_homepage_'. $storage]);
    variable_set('realname_nofollow_'. $storage, $form_state['values']['realname_nofollow_'. $storage]);
  }

  // Mark form for realname recalculation.
  variable_set('realname_recalculate', TRUE);

  drupal_set_message(t('Configuration has been updated.'), 'status');
}


function realname_admin_module_multiple($form_state) {


  $form = array();

  // Get the list of modules we support.
  include_once(drupal_get_path('module', 'realname') .'/realname_supported.inc');
  $supported_modules = realname_supported_modules();

  $choices = $show_types = array();

  foreach ($supported_modules as $module => $values) {
    if (module_exists($values['module'])) {
      $module_options[$module] = check_plain($values['name']);
    }
  }




  $form['realname_profile_module'] = array(
    '#type' => 'radios',
    '#title' => t('These modules are available for providing data to RealName'),
    '#options' => $module_options,
    '#default_value' => variable_get('realname_profile_module', 'NULL'),
    '#required' => TRUE,
  );


  if($form['realname_profile_module']['#default_value'][0] == NULL) {
    $text = t('You mush choose a module');
  }else {
    $text =  t('You have chosen the "@module" module to provide data.', array('@module' => 'por cumplimentar'));
  }


    $form['module'] = array(
      '#type' => 'item',
      //TODO ojo la primera vez no esta definida la variable. error de bootstraps.
      '#value' => $text,
    );


  $module = variable_get('realname_profile_module', NULL);
  if($module == 'content_profile') {

    //get the diferent profiles types
    $types = content_profile_get_types('names');

    // Now show the types, if appropriate.
    $form['realname_profile_type'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content types available to @module', array('@module' =>  $modulename)),
      '#options' => $types,
      '#default_value' => variable_get('realname_profile_type', array('NULL')),
      '#required' => TRUE,
    );
  }

  return system_settings_form($form);
}
