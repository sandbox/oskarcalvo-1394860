<?php
/**
 * @file
 * Realname module support for Profile (core) module.
 */

function realname_profile_get_fields($current) {

  $fields = $links = array();
  $result = db_query("SELECT name, type, title FROM {profile_fields}");
  while ($field = db_fetch_array($result)) {
    switch ($field['type']) {
      case 'selection':
      case 'textfield':
        $name = $field['name'];
        $selected = array_key_exists($name, $current);
        $fields['coreprofile'][$name] = array(
          'title' => $field['title'],
          'weight' => $selected ? $current[$name] : 0,
          'selected' => $selected,
          'origin'  => 'coreprofile',
          );
        break;

      case 'url':
        $links['coreprofile'][$field['name']] = $field['title'];
        break;

      default:
        break;
    }
  }

  return array('fields' => $fields, 'links' => $links);
}
