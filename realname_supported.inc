<?php
/**
 * @file
 * Realname module support list.
 */

function realname_supported_modules() {
  $list = array(
    'content_profile' => array(
      'name' => 'Content Profile',
      'module' => 'content_profile',
      'types' => TRUE,
      ),
    'coreprofile' => array(
      'name' => 'Core Profile',
      'module' => 'profile',
      'types' => FALSE,
      ),
    );


  return $list;
}

/**
 * @name
 *  _realname_clean_simple_array
 * @param
 *  $data: one simple array with one level only.
 *
 * @return
 *  the array withouth the Nulls values
 *
 * an example before
 * Array  (
 *   [profile] => profile
 *   [profile2] => profile2
 *   [profile3] => 0
 * )
 * after
 *  * Array  (
 *   [profile] => profile
 *   [profile2] => profile2
 * )
 */

function _realname_clean_simple_array($data){
  if(is_array($data)) {

    $newdata = array();

    foreach($data as $keydata => $valuedata) {

      if(!empty($valuedata)) {
        $newdata[$keydata] = $valuedata;
      }
    }

    return $newdata;
  }
  return;

}
